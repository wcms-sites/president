core = 7.x
api = 2
; addtoany
projects[addtoany][type] = "module"
projects[addtoany][download][type] = "git"
projects[addtoany][download][url] = "https://git.uwaterloo.ca/drupal-org/addtoany.git"
projects[addtoany][download][tag] = "7.x-4.0"
projects[addtoany][subdir] = ""

; uw_ct_honour
projects[uw_ct_honour][type] = "module"
projects[uw_ct_honour][download][type] = "git"
projects[uw_ct_honour][download][url] = "https://git.uwaterloo.ca/MSI/uw_ct_accolade.git"
projects[uw_ct_honour][download][tag] = "7.x-1.10"
projects[uw_ct_honour][subdir] = ""
